#ifndef WEBSERVER_HTTPREQUEST_H
#define WEBSERVER_HTTPREQUEST_H

#include <string>
#include <memory>
#include <unordered_map>

namespace Network {

    typedef std::unordered_map<std::string, std::string> RequestParams;


    class HttpRequest
    {
    private:
        enum class Type
        {
            HEAD, GET, PUT, POST
        };

        virtual ~HttpRequest(){};

        virtual Type getRequestParams() const = 0;
        virtual std::string const getHeaderAttr(const std::string &attrName) const = 0;

        virtual unsigned getContentSize() const  = 0;
        virtual void getContent(void *buffer, unsigned length, bool remove) const = 0;

        virtual std::string const getPath() const = 0;
        virtual RequestParams const getParams() const = 0;

        virtual void setResponseString(std::string const &str) = 0;
        virtual void setResponseBuffer(void const *data, unsigned bytes) = 0;
        virtual void setResponseFile(std::string const &fileName) = 0;
    };
}

#endif //WEBSERVER_HTTPREQUEST_H
