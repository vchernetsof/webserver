#ifndef WEBSERVER_NONCOPYABLE_H
#define WEBSERVER_NONCOPYABLE_H

namespace Common {
    class NonCopyable
    {
    protected:
        ~NonCopyable(){}

    public:
        NonCopyable(NonCopyable const &) = delete;
        NonCopyable &operator = (NonCopyable const &) = delete;
        NonCopyable() {}
    };
}

#endif //WEBSERVER_NONCOPYABLE_H
